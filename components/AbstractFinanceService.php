<?php
declare(strict_types=1);

namespace app\components;

use app\models\Deposit;

abstract class AbstractFinanceService
{

    /**
     * Calculate value of dividend or commission
     *
     * @param Deposit $deposit
     * @return float
     */
    abstract protected function getValueByPercentage(Deposit $deposit): float;

    /**
     * Calculated sum dividend or commission
     *
     * @param Deposit $deposit
     * @return float
     */
    protected function calculationValue(Deposit $deposit): float {
        return $deposit->value + $this->getValueByPercentage($deposit);
    }

}