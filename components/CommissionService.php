<?php

declare(strict_types=1);

namespace app\components;

use app\components\interfaces\CommissionServiceInterface;

class CommissionService extends AbstractFinanceService implements CommissionServiceInterface
{

    private const MIN_COMMISION = [
        'percent' => 5,
        'marginalValue' => 50];
    private const MID_COMMISION = [
        'percent' => 6];
    private const MAX_COMMISION = [
        'percent' => 7,
        'marginalValue' => 5000];

    private $firstDayPrevMonth;

    private $lastDayPrevMonth;

    private $firstDayObject;

    public function __construct(
        $firstDayPrevMonth,
        $lastDayPrevMonth,
        $firstDayObject
    )
    {
        $this->firstDayPrevMonth = $firstDayPrevMonth;
        $this->lastDayPrevMonth = $lastDayPrevMonth;
        $this->firstDayObject = $firstDayObject;
    }

    /**
     * Subtracting a commission
     *
     * @param \app\models\Deposit $deposit
     * @return float
     */
    public function calculationValue($deposit): float
    {
        return $deposit->value - $this->getValueByPercentage($deposit);
    }

    /**
     * Calculate value of commission
     *
     * @param \app\models\Deposit $deposit
     * @return float
     */
    protected function getValueByPercentage($deposit): float
    {
        list('percent' => $percent, 'marginalValue' => $commision) = $this->getPercentSize($deposit->value);

        $value = ($percent / 100) * $deposit->value;

        if ($percent == self::MIN_COMMISION['percent'] && $value < $commision) {
            $value = $commision;
        } elseif ($percent == self::MAX_COMMISION['percent'] && $value > $commision) {
            $value = $commision;
        }

        $countOfDaysIfUseLessMonth = $this->countOfDaysIfUseLessMonth((integer)$deposit->dateOpened);

        if ($countOfDaysIfUseLessMonth) {
            $value *= $countOfDaysIfUseLessMonth;
        }

        $value = \round($value, 2);

        return $value;
    }

    /**
     * Get percent of commission and marginal value
     *
     * @param float $value
     * @return array
     */
    private function getPercentSize(float $value): array
    {
        $result = [];

        if ($value >= 0 && $value < 1000) {
            $result = self::MIN_COMMISION;
        } else if ($value >= 1000 && $value < 10000) {
            $result = self::MID_COMMISION;
        } else if ($value >= 10000) {
            $result = self::MAX_COMMISION;
        }

        return $result;
    }

    /**
     * Counting the number of days if the client uses a deposit less than a month
     *
     * @param $dateOpened
     * @return float|bool
     */
    private function countOfDaysIfUseLessMonth($dateOpened)
    {
        if ($dateOpened >= $this->firstDayPrevMonth && $dateOpened < $this->lastDayPrevMonth) {

            $today = new \DateTime('now');
            $dateOpened = new \DateTime(date('d-m-Y', $dateOpened));

            $diff = $today->diff($dateOpened)->format("%a");
            $dayOfPrevMonth = $today->diff($this->firstDayObject)->format("%a");

            $days = $diff/$dayOfPrevMonth;
            return $days;
        }

        return false;
    }
}