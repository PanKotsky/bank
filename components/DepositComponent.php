<?php

declare(strict_types=1);

namespace app\components;

use app\components\interfaces\CommissionServiceInterface;
use app\components\interfaces\DepositComponentInterface;
use app\components\interfaces\DividendsServiceInterface;
use app\models\Deposit;
use yii\base\Component;

class DepositComponent extends Component implements DepositComponentInterface
{
    /**
     * Run process of calculating dividends and commission for deposits
     *
     * @param int $commision - if equal 1 - launch calculating commission if today first day of month
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function run(int $commision): bool
    {
        $deposits = $this->getAllDepositsForAddDividens();
        $service = \Yii::createObject(DividendsServiceInterface::class);
        echo "###DIVIDENDS###";

        foreach ($deposits as $deposit) {
            $value = $service->calculationValue($deposit);
            echo $deposit->id . ' ##### ' . $value . \PHP_EOL;
            $deposit->value = $value;
            if (!$deposit->save()) {
                throw new \RuntimeException('');
            }
        }

        if ((integer)\date('j', \time()) === 1 && $commision) {
            $this->runCommissions();
        }

        return true;
    }

    /**
     * Run process of calculating commission for deposits
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function runCommissions()
    {
        $depositsForComissions = Deposit::findAll(['active' => 1]);
        $firstDayPrevMonth = mktime(0,0,0,date("n")-1,1,date("Y"));
        $lastDayPrevMonth = mktime(0,0,0,date("n"),1,date("Y"));
        $firstDay = new \DateTime(\date('d-m-Y', $firstDayPrevMonth));
        $service = \Yii::createObject(CommissionServiceInterface::class, [$firstDayPrevMonth, $lastDayPrevMonth, $firstDay]);

        echo "###COMMISSIONS###";
        foreach ($depositsForComissions as $deposit) {
            $value = $service->calculationValue($deposit);
            echo $deposit->id . ' ##### ' . $value . \PHP_EOL;
            $deposit->value = $value;
            $deposit->save();
        }

        return true;
    }

    /**
     * Get All deposit for calculate dividends
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    private function getAllDepositsForAddDividens(): array
    {
//        $deposits = (new \yii\db\Query())
//            ->select(['*'])
//            ->from('deposit')
//            ->where("DATE_FORMAT(FROM_UNIXTIME(`dateOpened`), '%d') =". date('d', time()))
//            ->orWhere("DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP()), '%d') = ". \date('t') . " AND ".
//            "DATE_FORMAT(FROM_UNIXTIME(`dateOpened`), '%d') > ".\date("t"))
//            ->all();

//        $deposits = \Yii::$app->db->createCommand('SELECT * FROM deposit WHERE DATE_FORMAT(FROM_UNIXTIME(dateOpened), "%d") = '.\date("d", time()))
//            ->queryAll();

        $sql = 'SELECT * FROM deposit WHERE DATE_FORMAT(FROM_UNIXTIME(`dateOpened`), "%d") = '.\date("d", time()).'
            OR (DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP()), "%d") = '. \date("t") .' AND DATE_FORMAT(FROM_UNIXTIME(`dateOpened`), "%d") > '.\date("t").')';
        $deposits = Deposit::findBySql($sql)->all();

        return $deposits;
    }
}