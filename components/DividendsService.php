<?php

declare(strict_types = 1);

namespace app\components;

use app\components\interfaces\DividendsServiceInterface;
use app\models\Deposit;

class DividendsService extends AbstractFinanceService implements DividendsServiceInterface
{
    /**
     * Calculate dividend
     *
     * @param Deposit $deposit
     * @return float|int
     */
    protected function getValueByPercentage(Deposit $deposit): float
    {
        $value = ($deposit->percent / 100) * $deposit->value;

        $value = \round($value, 2);

        return $value;
    }
}