<?php

declare(strict_types=1);

namespace app\components\interfaces;

interface DepositComponentInterface
{
    /**
     * Run process of calculation dividends and commissions
     *
     * @param int $commision
     *
     * @return bool
     */
    public function run(int $commision): bool;
    
    /**
     * Run process of calculating commission for deposits
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function runCommissions(): bool
}