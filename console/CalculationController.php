<?php

namespace app\console;

use app\components\interfaces\DepositComponentInterface;

class CalculationController extends \yii\console\Controller
{

    /**
     * Run process calculating dividends (and commissions if today first day of month)
     *
     * @param int $commission
     */
    public function actionRun(int $commission = 0)
    {
        try {

            $service = \Yii::createObject(DepositComponentInterface::class);

            if (!$service->run($commission)) {
                throw new \RuntimeException('Something went wrong when calculating dividends');
            }

        } catch (\Throwable $t) {
            echo $t->getMessage();
        }
    }

    /**
     *  Run process calculating commissions if we want to run a separate process
     */
    public function actionRunCommission()
    {
        try {

            $service = \Yii::createObject(DepositComponentInterface::class);

            if (!$service->runCommissions()) {
                throw new \RuntimeException('Something went wrong when calculating commissions');
            }

        } catch (\Throwable $t) {
            echo $t->getMessage();
        }
    }
}