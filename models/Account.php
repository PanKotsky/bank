<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "account".
 *
 * @property int $id
 * @property int $clientId
 * @property bool $active
 * @property double $value
 * @property string $dateOpened
 * @property string $dateClosed
 *
 * @property Client $client
 * @property Deposit[] $deposit
 */
class Account extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clientId', 'active', 'dateOpened'], 'required'],
            [['clientId'], 'integer'],
            [['value'], 'number'],
            [['dateOpened', 'dateClosed'], 'filter', 'filter' => function ($value) {
                if (empty($value)) {
                    return '';
                }
                return (string)\strtotime($value);
            }],
            [['dateOpened', 'dateClosed'], 'string', 'max' => 10],
            [['active'], 'boolean'],
            [['clientId'], 'exist', 'skipOnError' => true, 'targetClass' => Client::class, 'targetAttribute' => ['clientId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Client ID'),
            'active' => Yii::t('app', 'Active'),
            'value' => Yii::t('app', 'Value'),
            'dateOpened' => Yii::t('app', 'Date Opened'),
            'dateClosed' => Yii::t('app', 'Date Closed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'clientId']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeposit()
    {
        return $this->hasMany(Deposit::class, ['accountId' => 'id']);
    }
}
