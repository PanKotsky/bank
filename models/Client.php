<?php

declare(strict_types = 1);

namespace app\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property int $genderId
 * @property string $birthday
 * @property string $itin Individual Taxpayer Identification Number
 *
 * @property Account[] $accounts
 * @property Gender $gender
 * @property History[] $histories
 */
class Client extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'gender', 'birthday', 'itin'], 'required'],
            [['genderId'], 'integer'],
            [['firstName', 'lastName'], 'string', 'max' => 90],
            [['birthday'], 'string', 'max' => 10],
            [['itin', 'birthday'], 'string', 'max' => 10],
            [['genderId'], 'exist', 'skipOnError' => true, 'targetClass' => Gender::class, 'targetAttribute' => ['gender' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstName' => Yii::t('app', 'First Name'),
            'lastName' => Yii::t('app', 'Last Name'),
            'genderId' => Yii::t('app', 'Gender'),
            'birthday' => Yii::t('app', 'Birthday'),
            'itin' => Yii::t('app', 'Individual Taxpayer Identification Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::class, ['clientId' => 'id'])->inverseOf('client');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(Gender::class, ['id' => 'genderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::class, ['clientId' => 'id'])->inverseOf('client');
    }
}
