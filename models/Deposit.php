<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deposit".
 *
 * @property int $id
 * @property int $accountId
 * @property bool $active
 * @property double $value
 * @property double $percent
 * @property double $commission percent
 * @property string $dateOpened
 * @property string $dateClosed
 *
 * @property Account $account
 */
class Deposit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deposit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['accountId', 'active', 'percent', 'commission', 'dateOpened'], 'required'],
            [['accountId'], 'integer'],
            [['value', 'percent', 'commission'], 'number'],
            [['dateOpened', 'dateClosed'], 'filter', 'filter' => function ($value) {
                if (empty($value)) {
                    return '';
                }
                return (string)\strtotime($value);
            }],
            [['dateOpened', 'dateClosed'], 'string', 'max' => 10],
            [['active'], 'boolean'],
            [['accountId'], 'exist', 'skipOnError' => true, 'targetClass' => Account::class, 'targetAttribute' => ['accountId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'accountId' => Yii::t('app', 'Account ID'),
            'active' => Yii::t('app', 'Active'),
            'value' => Yii::t('app', 'Value'),
            'percent' => Yii::t('app', 'Percent'),
            'commission' => Yii::t('app', 'Commission'),
            'dateOpened' => Yii::t('app', 'Date Opened'),
            'dateClosed' => Yii::t('app', 'Date Closed'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::class, ['id' => 'accountId']);
    }
}
