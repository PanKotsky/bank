<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property int $clientId
 * @property int $accountId
 * @property string $action
 * @property double $value
 * @property double $remainder
 * @property double $dateOperation
 *
 * @property Client $client
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clientId', 'accountId', 'action', 'value', 'remainder', 'dateOperation'], 'required'],
            [['clientId', 'accountId'], 'integer'],
            [['value', 'remainder', 'dateOperation'], 'number'],
            [['action'], 'string', 'max' => 45],
            [['clientId'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['clientId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'clientId' => Yii::t('app', 'Client ID'),
            'accountId' => Yii::t('app', 'Account ID'),
            'action' => Yii::t('app', 'Action'),
            'value' => Yii::t('app', 'Value'),
            'remainder' => Yii::t('app', 'Remainder'),
            'dateOperation' => Yii::t('app', 'Date Operation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'clientId']);
    }
}
