<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $client app\models\Client */
/* @var $gender array */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($client, 'firstName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($client, 'lastName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($client, 'genderId')->dropDownList($gender, ['prompt' => 'Choose gender...',[
        'disabled' => true,
    ]]) ?>

    <?= $form->field($client, 'birthday')->input('date') ?>

    <?= $form->field($client, 'itin')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
