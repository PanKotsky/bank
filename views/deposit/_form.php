<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Deposit */
/* @var $form yii\widgets\ActiveForm */
/* @var $account int */
?>

<div class="deposit-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($model, 'accountId')->textInput() ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'percent')->textInput() ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'commission')->textInput() ?>

    <?= $form->field($model, 'dateOpened')->input('date') ?>

    <?= $form->field($model, 'dateClosed')->input('date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
